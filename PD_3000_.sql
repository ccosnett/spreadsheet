use valdev_6;

SELECT latitude4326, longitude4326 FROM liqquid_addresshub WHERE  postcode='N13 5PD';

SET @t_lat = 51.6210646000; # 51.6185360000;
SET @t_lon = -0.1058045000; #-0.0995751000#;
SET @search_radius = 0.009*0.6120;


SELECT COUNT(aid) FROM liqquid_addresshub ah WHERE MBRCONTAINS(LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius)), ah.coords);

DROP TABLE IF EXISTS PD3000;
CREATE TABLE PD3000 AS
    SELECT ah.aid, #valdev
           ah.UPRN AS UPRN,
           ah.UDPRN AS UDPRN,
           ah.organisationName AS organisationName,
           ah.buildingNumber AS buildingNumber,
           ah.buildingName AS buildingName,
           ah.subBuildingName AS subBuildingName,
           ah.dependentLocality AS dependentLocality,
           ah.doubleDependentLocality AS doubleDependentLocality,
           ah.thoroughfare AS thoroughfare,
           ah.dependentThoroughfare AS dependentThoroughfare,
           ah.departmentName AS departmentName,
           ah.postTown AS postTown,
           ah.postcode AS postcode,
           ah.postcodeType AS postcodeType,
           ah.address1Line AS address1Line,
           ah.propertyType AS propertyType,
           ah.PropTypeCategory AS PropTypeCategory,
           ah.latitude4326 AS latitude4326,
           ah.longitude4326 AS longitude4326,
           ah.ranked_number AS ranked_number,
           ah.plotSize AS plotSize,
           ah.tenure AS tenure,
           ah.adminCountry AS adminCountry,
           ah.adminCountryCode AS adminCountryCode,
           ah.adminCounty AS adminCounty,
           ah.adminCountyCode AS adminCountyCode,
           ah.adminDistrict AS adminDistrict,
           ah.adminDistrictCode AS adminDistrictCode,
           ah.adminDistrictType AS adminDistrictType,
           ah.adminWard AS adminWard,
           ah.adminWardCode AS adminWardCode,
           ah.adminWardType AS adminWardType,
           ah.registeredBusiness AS registeredBusiness,
           ah.estimated_geo AS estimated_geo,
           ah.sub_ranked_number AS sub_ranked_number,
           ah.estimated_rank AS estimated_rank,
           ah.coords AS coords,
           ah.aid AS aid_,
           TransactionId,
           Price,
           TransactionDate,
           pp.PropertyType As PropertyTYpe_,
           OldNew,
           PPD,
           Duration,
           District,
           LMK_KEY,
           BUILDING_REFERENCE_NUMBER,
           CURRENT_ENERGY_RATING,
           CURRENT_ENERGY_EFFICIENCY,
           PROPERTY_TYPE,
           BUILT_FORM,
           INSPECTION_DATE,
           LODGEMENT_DATE,
           TRANSACTION_TYPE,
           TOTAL_FLOOR_AREA,
           NUMBER_HABITABLE_ROOMS,
           WINDOWS_DESCRIPTION,
           CONSTRUCTION_AGE_BAND,
           LODGEMENT_DATETIME
    FROM liqquid_addresshub ah
    LEFT JOIN liqquid_pp pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp pp WHERE pp.aid=ah.aid)
    LEFT JOIN liqquid_epc_certs epc ON ah.aid = epc.aid
WHERE MBRCONTAINS(LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius)), ah.coords)
LIMIT 3000;
SELECT * FROM PD3000;
#ALTER TABLE table_name RENAME old_col_name TO new_col_name;

aid,UPRN,UDPRN,organisationName,buildingNumber,buildingName,subBuildingName,dependentLocality,doubleDependentLocality,thoroughfare,dependentThoroughfare,departmentName,postTown,postcode,postcodeType,address1Line,propertyType,PropTypeCategory,latitude4326,longitude4326,ranked_number,plotSize,tenure,adminCountry,adminCountryCode,adminCounty,adminCountyCode,adminDistrict,adminDistrictCode,adminDistrictType,adminWard,adminWardCode,adminWardType,registeredBusiness,estimated_geo,sub_ranked_number,estimated_rank,coords,aid ,TransactionId,Price,TransactionDate,PropertyType	,OldNew,PPD,Duration,District,LMK_KEY,BUILDING_REFERENCE_NUMBER,CURRENT_ENERGY_RATING,CURRENT_ENERGY_EFFICIENCY,PROPERTY_TYPE,BUILT_FORM,INSPECTION_DATE,LODGEMENT_DATE,TRANSACTION_TYPE,TOTAL_FLOOR_AREA,NUMBER_HABITABLE_ROOMS,WINDOWS_DESCRIPTION,CONSTRUCTION_AGE_BAND,LODGEMENT_DATETIME#ss
aid,UPRN,UDPRN,organisationName,buildingNumber,buildingName,subBuildingName,dependentLocality,doubleDependentLocality,thoroughfare,dependentThoroughfare,departmentName,postTown,postcode,postcodeType,address1Line,propertyType,PropTypeCategory,latitude4326,longitude4326,ranked_number,plotSize,tenure,adminCountry,adminCountryCode,adminCounty,adminCountyCode,adminDistrict,adminDistrictCode,adminDistrictType,adminWard,adminWardCode,adminWardType,registeredBusiness,estimated_geo,sub_ranked_number,estimated_rank,coords,aid_,TransactionId,Price,TransactionDate,PropertyTYpe_,OldNew,PPD,Duration,District,LMK_KEY,BUILDING_REFERENCE_NUMBER,CURRENT_ENERGY_RATING,CURRENT_ENERGY_EFFICIENCY,PROPERTY_TYPE,BUILT_FORM,INSPECTION_DATE,LODGEMENT_DATE,TRANSACTION_TYPE,TOTAL_FLOOR_AREA,NUMBER_HABITABLE_ROOMS,WINDOWS_DESCRIPTION,CONSTRUCTION_AGE_BAND,LODGEMENT_DATETIME#db
