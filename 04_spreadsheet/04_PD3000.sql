use valdev_4;

#51.733470, -1.226226

#SELECT latitude4326, longitude4326 FROM liqquid_addresshub WHERE buildingNumber='72' AND postcode='OX4 3BE';
SELECT latitude4326, longitude4326 FROM liqquid_addresshub WHERE buildingNumber='41' AND postcode='CH44 4DR';


#SET @t_lat = 51.733470;
SET @t_lat = 53.4134;
#SET @t_lon = -1.226226;
SET @t_lon = -3.04005;
SET @search_radius = 0.009*0.65;
#SET @search_radius = 0.018;

#SELECT LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius));

SELECT COUNT(aid) FROM liqquid_addresshub ah WHERE MBRCONTAINS(LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius)), ah.coords);
#SET @search_radius = 0.001;
DROP TABLE IF EXISTS PD3000;
CREATE TABLE PD3000 AS
    SELECT vla.aid AS aid, #valdev
           ah.UPRN AS UPRN,
           ah.UDPRN AS UDPRN,
           ah.organisationName AS organisationName,
           ah.buildingNumber AS buildingNumber,
           ah.buildingName AS buildingName,
           ah.subBuildingName AS subBuildingName,
           ah.dependentLocality AS dependentLocality,
           ah.doubleDependentLocality AS doubleDependentLocality,
           ah.thoroughfare AS thoroughfare,
           ah.dependentThoroughfare AS dependentThoroughfare,
           ah.departmentName AS departmentName,
           ah.postTown AS postTown,
           ah.postcode AS postcode,
           ah.postcodeType AS postcodeType,
           ah.address1Line AS address1Line,
           ah.propertyType AS propertyType,
           ah.PropTypeCategory AS PropTypeCategory,
           ah.latitude4326 AS latitude4326,
           ah.longitude4326 AS longitude4326,
           ah.ranked_number AS ranked_number,
           ah.plotSize AS plotSize,
           ah.tenure AS tenure,
           ah.adminCounty AS adminCounty,
           ah.adminCountyCode AS adminCountyCode,
           ah.adminDistrict AS adminDistrict,
           ah.adminDistrictCode AS adminDistrictCode,
           ah.adminDistrictType AS adminDistrictType,
           ah.adminWard AS adminWard,
           ah.adminWardCode AS adminWardCode,
           ah.adminWardType AS adminWardType,
           ah.registeredBusiness AS registeredBusiness,
           ah.estimated_geo AS estimated_geo,
           ah.sub_ranked_number AS sub_ranked_number,
           ah.estimated_rank AS estimated_rank,
           ah.coords AS coords,
           vla.aid AS aid_,
           TransactionId,
           Price,
           TransactionDate,
           pp.PropertyType As PropertyTYpe_,
           OldNew,
           PPD,
           Duration,
           District
           LMK_KEY,
           BUILDING_REFERENCE_NUMBER,
           CURRENT_ENERGY_RATING,
           CURRENT_ENERGY_EFFICIENCY,
           PROPERTY_TYPE,
           BUILT_FORM,
           INSPECTION_DATE,
           LODGEMENT_DATE,
           TRANSACTION_TYPE,
           TOTAL_FLOOR_AREA,
           NUMBER_HABITABLE_ROOMS,
           WINDOWS_DESCRIPTION,
           CONSTRUCTION_AGE_BAND,
           LODGEMENT_DATETIME
    FROM liqquid_addresshub ah
    LEFT JOIN liqquid_pp pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp pp WHERE pp.aid=ah.aid)
    LEFT JOIN liqquid_epc_certs epc ON ah.aid = epc.aid
    LEFT JOIN aids vla ON ah.aid=vla.aid4
WHERE MBRCONTAINS(LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius)), ah.coords);
SELECT * FROM PD3000;
DESCRIBE PD3000;

SELECT * FROM liqquid_addresshub where postcode= 'ch44 4dr'



SELECT * FROM

SELECT * FROM PD3000;

SELECT * FROM valdev.liqquid_addresshub where aid = 13344610;

SELECT * FROM valdev_2.liqquid_epc_certs where aid = 4196;


SELECT * FROM valdev_2.liqquid_ah_epc_ALL where tid = 698443030312012041019511097920591;
SELECT * FROM valdev_2.liqquid_ah_epc_ALL map RIGHT JOIN valdev_2.liqquid_epc_certs_ALL certs ON map.tid=certs.LMK_KEY  where address='33 kingsley road, wallasey, ch44 4dr';
################################

SELECT * FROM valdev_3.liqquid_epc_certs_ALL_TEST;
SELECT '';
SELECT * FROM aids;
###############################


RENAME TABLE valdev_2.pp To valdev_2.ppp;


SELECT * FROM valdev_4.liqquid_addresshub where address1Line LIKE '41 kingsley road, wallasey, ch44 4dr';



CREATE TABLE valdev_2.pp AS SELECT * FROM valdev_2.liqquid_pp WHERE aid!=0;
LEFT JOIN liqquid_epc_certs epc ON ah.aid = epc.aid


SELECT * FROM valdev_3.liqquid_addresshub;
#SELECT * FROM valdev_2.liqquid_addresshub WHERE aid = 4200;
#SELECT * FROM valdev.liqquid_addresshub WHERE aid = 22412787;



SELECT count(*) FROM valdev_3.liqquid_pp WHERE aid !=0;


CREATE INDEX;


DESCRIBE valdev_3.liqquid_pp;



SELECT * from valdev_2.liqquid_EA_PORT_data LIMIT 1;

SELECT COUNT(*) FROM valdev.liqquid_EA_PORT_data leapd;

SELECT * FROM valdev_3.liqquid_EA_PORT_data_0;


use valdev_3;

CREATE TABLE liqquid_EA_PORT_data_0 (
  `url` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `category` varchar(45) DEFAULT NULL,
  `dateadded` datetime DEFAULT NULL,
  `dateupdated` datetime DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `bedrooms` int(11) DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `floors` int(11) DEFAULT NULL,
  `price` varchar(45) DEFAULT NULL,
  `pcm` varchar(45) DEFAULT NULL,
  `pw` varchar(45) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `epclink` varchar(255) DEFAULT NULL,
  `keyFeatures` text,
  `description` text,
  `agent` varchar(255) DEFAULT NULL,
  `agentaddress` varchar(255) DEFAULT NULL,
  `agentphone` varchar(15) DEFAULT NULL,
  `Type1` varchar(1) DEFAULT NULL,
  `Type2` varchar(1) DEFAULT NULL,
  `Type3` varchar(1) DEFAULT NULL,
  `Type4` varchar(2) DEFAULT NULL,
  `postcode` varchar(9) DEFAULT NULL,
  `thoroughfare` varchar(255) DEFAULT NULL,
  `aid` int(10) DEFAULT NULL,
  `coords` point NOT NULL,
  `address` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE liqquid_EA_PORT_data_0 (
  `url` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `category` varchar(45) DEFAULT NULL,
  `dateadded` datetime DEFAULT NULL,
  `dateupdated` datetime DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `bedrooms` int(11) DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `floors` int(11) DEFAULT NULL,
  `price` varchar(45) DEFAULT NULL,
  `pcm` varchar(45) DEFAULT NULL,
  `pw` varchar(45) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `epclink` varchar(255) DEFAULT NULL,
  `keyFeatures` text,
  `description` text,
  `agent` varchar(255) DEFAULT NULL,
  `agentaddress` varchar(255) DEFAULT NULL,
  `agentphone` varchar(15) DEFAULT NULL,
  `Type1` varchar(1) DEFAULT NULL,
  `Type2` varchar(1) DEFAULT NULL,
  `Type3` varchar(1) DEFAULT NULL,
  `Type4` varchar(2) DEFAULT NULL,
  `postcode` varchar(9) DEFAULT NULL,
  `thoroughfare` varchar(255) DEFAULT NULL,
  `aid` int(10) DEFAULT NULL,
  `coords` point NOT NULL,
  `address` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




USE valdev_2;
CREATE TABLE liqquid_ah_epc_LATEST_LODGEMENT_DATE AS SELECT ah.aid, ah.address1Line, ahepc.address, ahepc.tid, epc.LMK_KEY, epc.LODGEMENT_DATE/*, MAX(epc.LODGEMENT_DATE)*/ FROM
valdev_2.liqquid_addresshub ah
    INNER JOIN valdev_2.liqquid_ah_epc_ALL ahepc ON ah.address1Line = ahepc.address
    INNER JOIN valdev_2.liqquid_epc_certs_ALL epc ON ahepc.tid = epc.LMK_KEY
WHERE address1Line LIKE '%ch44 4dr';



USE valdev_2;
CREATE TABLE liqquid_ah_epc_LODGEMENT_DATE_s AS SELECT ah.aid, ah.address1Line, ahepc.address, ahepc.tid, epc.LMK_KEY, epc.LODGEMENT_DATE/*, MAX(epc.LODGEMENT_DATE)*/ FROM
valdev_2.liqquid_addresshub ah
    INNER JOIN valdev_2.liqquid_ah_epc_ALL ahepc ON ah.address1Line = ahepc.address
    INNER JOIN valdev_2.liqquid_epc_certs_ALL epc ON ahepc.tid = epc.LMK_KEY;



###############################################################################################################################################################################
###############################################################################################################################################################################
SELECT laeLLD.*, lecA.* FROM (SELECT * FROM liqquid_ah_epc_LATEST_LODGEMENT_DATE WHERE address1Line like '41 kingsley road,% ch44 4dr') laeLLD INNER JOIN liqquid_epc_certs_ALL lecA ON laeLLD.LMK_KEY=lecA.LMK_KEY;

SELECT * FROM liqquid_ah_epc_MAX_LODGEMENT_DATE WHERE address1Line like '41 kingsley road,% ch44 4dr';

###############################################################################################################################################################################
###############################################################################################################################################################################
drop table liqquid_ah_epc_MAX_LODGEMENT_DATE;
CREATE TABLE liqquid_ah_epc_MAX_LODGEMENT_DATE AS SELECT aid, address1Line, LMK_KEY, MAX(LODGEMENT_DATE) AS 'MAX_LODGEMENT_DATE' FROM valdev_2.liqquid_ah_epc_LATEST_LODGEMENT_DATE
GROUP BY aid;

SELECT * FROM (SELECT *, MAX(LODGEMENT_DATE) AS 'MAX_LODGEMENT_DATE' FROM ch44 GROUP BY aid, address1Line, address ) s WHERE address1Line LIKE '41% 4dr';
















SELECT * FROM liqquid_epc_certs_ALL;



###############################################################################################################################################################################
###############################################################################################################################################################################
SELECT * FROM (SELECT lec.*, laeMLD.aid as laeMLD_aid, laeMLD.MAX_LODGEMENT_DATE, laeMLD.LMK_KEY as laeMLD_LMK_KEY, laeMLD.address1Line from liqquid_ah_epc_MAX_LODGEMENT_DATE laeMLD INNER JOIN liqquid_epc_certs_ALL lec ON lec.LMK_KEY=laeMLD.LMK_KEY) tt;
/*CREATE TABLE liqquid_epc_certs_NEW AS*/

SELECT * FROM
              (SELECT lec.*,laeMLD.MAX_LODGEMENT_DATE, laeMLD.address1Line, laeMLD.LMK_KEY AS laeMLD_LMK_KEY from liqquid_ah_epc_MAX_LODGEMENT_DATE laeMLD INNER JOIN liqquid_epc_certs_ALL lec ON lec.LMK_KEY=laeMLD.LMK_KEY) tt
WHERE address1Line like '41 king%';

SELECT * from liqquid_epc_certs_NEW WHERE address1Line like '%ch44 4dr';
##############################################################################################################################################################################################################################################################################################################################################################
###############################################################################################################################################################################
SELECT * FROM liqquid_ah_epc_MAX_LODGEMENT_DATE;

SELECT aid, LODGEMENT_DATE, liqquid_epc_certs. FROM valdev_2.liqquid_epc_certs group by aid;

SELECT la.aid, LODGEMENT_DATE, address1Line FROM liqquid_epc_certs INNER JOIN liqquid_addresshub la on liqquid_epc_certs.aid = la.aid GROUP BY la.aid;


(`LMK_KEY`,
`BUILDING_REFERENCE_NUMBER`,
`CURRENT_ENERGY_RATING`,
`CURRENT_ENERGY_EFFICIENCY`,
`PROPERTY_TYPE`,
`BUILT_FORM`,
`INSPECTION_DATE`,
`LODGEMENT_DATE`,
`TRANSACTION_TYPE`,
`TOTAL_FLOOR_AREA`,
`NUMBER_HABITABLE_ROOMS`,
`WINDOWS_DESCRIPTION`,
`CONSTRUCTION_AGE_BAND`,
`LODGEMENT_DATETIME`,
`aid`,
`EPC_ownership`)


GROUP BY address1Line, address;


CREATE TABLE

    /*AND ahepc.tid > epc.LMK_KEY*/
/*WHERE ah.address1Line LIKE '41 kingsley road%'*/




select * from liqquid_epc_certs_ALL;


        INNER JOIN liqquid_epc_certs_ALL epc ON ahepc.tid = epc.LMK_KEY
        LEFT JOIN liqquid_ah_epc_ALL epc2 ON ahepc.address = epc2.address AND ahepc.tid < epc2.tid

;





SELECT * FROM valdev_2.liqquid_addresshub;

set epc.aid = ah.aid
where  epc2.tid is null;


SELECT COUNT(*) FROM avm_postcode_type_price_stats_2;









SELECT vla.aid AS aid, #valdev
           ah.UPRN AS UPRN,
           ah.UDPRN AS UDPRN,
           ah.organisationName AS organisationName,
           ah.buildingNumber AS buildingNumber,
           ah.buildingName AS buildingName,
           ah.subBuildingName AS subBuildingName,
           ah.dependentLocality AS dependentLocality,
           ah.doubleDependentLocality AS doubleDependentLocality,
           ah.thoroughfare AS thoroughfare,
           ah.dependentThoroughfare AS dependentThoroughfare,
           ah.departmentName AS departmentName,
           ah.postTown AS postTown,
           ah.postcode AS postcode,
           ah.postcodeType AS postcodeType,
           ah.address1Line AS address1Line,
           ah.propertyType AS propertyType,
           ah.PropTypeCategory AS PropTypeCategory,
           ah.latitude4326 AS latitude4326,
           ah.longitude4326 AS longitude4326,
           ah.ranked_number AS ranked_number,
           ah.plotSize AS plotSize,
           ah.tenure AS tenure,
           ah.adminCounty AS adminCounty,
           ah.adminCountyCode AS adminCountyCode,
           ah.adminDistrict AS adminDistrict,
           ah.adminDistrictCode AS adminDistrictCode,
           ah.adminDistrictType AS adminDistrictType,
           ah.adminWard AS adminWard,
           ah.adminWardCode AS adminWardCode,
           ah.adminWardType AS adminWardType,
           ah.registeredBusiness AS registeredBusiness,
           ah.estimated_geo AS estimated_geo,
           ah.sub_ranked_number AS sub_ranked_number,
           ah.estimated_rank AS estimated_rank,
           ah.coords AS coords,
           vla.aid AS aid_,
           TransactionId,
           Price,
           TransactionDate,
           pp.PropertyType As PropertyTYpe_,
           OldNew,
           PPD,
           Duration,
           District
           LMK_KEY,
           BUILDING_REFERENCE_NUMBER,
           CURRENT_ENERGY_RATING,
           CURRENT_ENERGY_EFFICIENCY,
           PROPERTY_TYPE,
           BUILT_FORM,
           INSPECTION_DATE,
           LODGEMENT_DATE,
           TRANSACTION_TYPE,
           TOTAL_FLOOR_AREA,
           NUMBER_HABITABLE_ROOMS,
           WINDOWS_DESCRIPTION,
           CONSTRUCTION_AGE_BAND,
           LODGEMENT_DATETIME
    FROM liqquid_addresshub ah
    LEFT JOIN liqquid_pp pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp pp WHERE pp.aid=ah.aid)
    LEFT JOIN liqquid_epc_certs epc ON ah.aid = epc.aid
    LEFT JOIN aids vla ON ah.aid=vla.aid4
WHERE MBRCONTAINS(LINESTRING(POINT(-3.04005 + 0.009, 53.4134 + 0.009),  POINT(-3.04005 - 0.009, 53.4134 - 0.009)), ah.coords);