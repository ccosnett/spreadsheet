use valdev_4;
SET @t_lat = 53.4134401000;
SET @t_lon = -3.0400469000;
SET @search_radius = 0.0517;

SET @t_lat = 51.733470;
SET @t_lon = -1.226226;
SET @search_radius = 0.4;


######################################################################################################################################################################################
######################################################################################################################################################################################
######################################################################################################################################################################################
select count(*) from valdev_4_rm_1000;

DROP TABLE valdev_4_rm_1000;
CREATE TABLE valdev_4_rm_1000 AS
                                   SELECT * FROM liqquid_EA_PORT_data leap
WHERE MBRCONTAINS(LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius)), leap.coords);

######################################################################################################################################################################################
######################################################################################################################################################################################
######################################################################################################################################################################################


DESCRIBE valdev_3.liqquid_EA_PORT_data;


DROP TABLE rm_1000_aid;
CREATE TABLE rm_1000_aid AS (
    SELECT distinct S.aid FROM
                     (
                         SELECT rm.id,
                             rm.AID FROM rm_matches rm
LEFT JOIN liqquid_addresshub ah ON ah.aid=rm.aid
WHERE MBRCONTAINS(LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius)), coords)
                         )
                         S);



DROP TABLE rm_1000_aid;
CREATE TABLE rm_1000_aid AS (
    SELECT distinct S.aid FROM
                     (
                         SELECT rm.id,
                             rm.AID FROM rm_matches rm
LEFT JOIN liqquid_addresshub ah ON ah.aid=rm.aid
WHERE MBRCONTAINS(LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius)), coords)
                         )
                         S);


SELECT COUNT(*) FROM rm_1000_aid;
#SELECT distinct aid FROM valdev_4.rm_matches;


SELECT COUNT(*) FROM liqquid_EA_PORT_data;

CREATE TABLE union1 AS SELECT l.url,
       l.id AS id,
       l.category AS category,
       l.dateadded AS dateadded,
       l.dateupdated AS date,
       l.type AS type,
       l.bedrooms AS bedrooms,
       l.bathrooms AS bathrooms,
       l.floors AS floors,
       l.price AS price,
       l.lat AS lat,
       l.lon AS lon,
       leap.epclink AS epclink,
       leap.keyFeatures AS keyFeatures,
       leap.description AS description,
       leap.agent AS agent,
       leap.agentaddress AS agentaddress,
       leap.agentphone AS agentphone,
       ah.aid AS aid,
       ah.postcode AS postcode,
       ah.thoroughfare AS thooughfare,
       ah.postTown AS postTown
       FROM rm_1000_aid ra
    INNER JOIN rm_matches rm ON ra.aid=rm.aid
    INNER JOIN valdev_3.liqquid_EA_PORT_data_2 l ON l.id = rm.id
    INNER JOIN valdev_3.liqquid_EA_PORT_data leap ON leap.id=rm.id
    INNER JOIN valdev.liqquid_addresshub ah ON ah.address1Line=rm.address;

RENAME TABLE liqquid_EA_PORT_data TO liqquid_EA_PORT_data_0;
CREATE INDEX ind10 ON liqquid_EA_PORT_data_0(id);
CREATE INDEX ind10 ON liqquid_EA_PORT_data_2(id);
CREATE INDEX ind10 ON liqquid_addresshub(address1Line);
DROP TABLE IF EXISTS liqquid_EA_PORT_data;
CREATE TABLE liqquid_EA_PORT_data AS
    SELECT
           l.url,
           l.id AS id,
           l.category AS category,
           l.dateadded AS dateadded,
           l.dateupdated AS date,
           l.type AS type,
           l.bedrooms AS bedrooms,
           l.bathrooms AS bathrooms,
           l.floors AS floors,
           l.price AS price,
           leap.pcm AS pcm,
           leap.pw AS pw,
           l.lat AS lat,
           l.lon AS lon,
           leap.epclink AS epclink,
           leap.keyFeatures AS keyFeatures,
           leap.description AS description,
           leap.agent AS agent,
           leap.agentaddress AS agentaddress,
           leap.agentphone AS agentphone,
           l.Type1 AS Type1,
           l.Type2 AS Type2,
           l.Type3 AS Type3,
           l.Type4 AS Type4,
           ah.postcode AS postcode,
           ah.thoroughfare AS thooughfare,
           ah.aid AS aid,
           leap.coords AS coords,
           ah.address1Line AS address,
           ah.postTown AS postTown
    FROM  RM_id_aid_address rm
    INNER JOIN liqquid_EA_PORT_data_2 l ON l.id = rm.id
    INNER JOIN liqquid_EA_PORT_data_0 leap ON leap.id=rm.id
    INNER JOIN liqquid_addresshub ah ON ah.address1Line=rm.address;
RENAME TABLE liqquid_EA_PORT_data TO liqquid_EA_PORT_data_0;

RENAME TABLE union3 TO liqquid_EA_PORT_data;
RENAME TABLE liqquid_EA_PORT_data TO liqquid_EA_PORT_data_OLD_AIDs;
SELECT * FROM liqquid_EA_PORT_data;

SELECT * FROM valdev.liqquid_EA_PORT_data;

SELECT COUNT(*) FROM (SELECT subquery1.* FROM (SELECT DISTINCT aid FROM union1) subquery1
INNER JOIN union1 u WHERE subquery1.aid=u.aid) subquery2;

SELECT COUNT(*) FROM RM_id_aid_address;
SELECT * FROM (SELECT * FROM RM_id_aid_address group by aid) S;

ALTER TABLE RM_id_aid_address MODIFY COLUMN address varchar(255);

DESCRIBE liqquid_addresshub;
CREATE INDEX ind0 ON RM_id_aid_address(id, aid);
#CREATE INDEX ind0 ON RM_id_aid_address(address);

SELECT * FROM (SELECT * FROM union1 GROUP BY aid) S;

SELECT COUNT(*) FROM valdev.liqquid_thoroughfare_groups;

select * from valdev.liqquid_ukhpi where Date = '2007-04-01' and areacode = 'e08000015';
select * from valdev.liqquid_ukhpi where Date = '2007-04-01';


SELECT * from information_schema.PROCESSLIST WHERE DB='valdev_4';
KILL 33614270;
KILL 33614229;

Select concat('KILL ',id,';') from information_schema.processlist where db='valdev';
"concat('KILL ',id,';')"
"concat('KILL ',id,';')"
KILL 33457620;
KILL 33457631;

"concat('KILL ',id,';')"
KILL 33614222;
KILL 33614096;
KILL 33614256;
KILL 33614213;
KILL 33614258;
KILL 33613948;
KILL 33614093;
KILL 33614207;
KILL 33614088;
KILL 33614171;
KILL 33614097;
KILL 33614015;
KILL 33614200;
KILL 33614056;
KILL 33614057;
KILL 33614155;
KILL 33614236;
KILL 33614091;
KILL 33614018;
KILL 33613960;
KILL 33614204;
KILL 33614250;
KILL 33613966;
KILL 33614254;
KILL 33614152;
KILL 33614205;
KILL 33614125;
KILL 33613980;
KILL 33614186;
KILL 33614036;
KILL 33614238;


KILL 33614268;
KILL 33614239;
KILL 33614269;
KILL 33614219;
KILL 33614230;
KILL 33614168;
KILL 33614231;
KILL 33614145;
KILL 33614154;
KILL 33614172;
KILL 33614226;
KILL 33614232;
KILL 33614233;
KILL 33614185;
KILL 33614224;
KILL 33614223;
KILL 33614237;
KILL 33614178;
KILL 33614240;
KILL 33614146;
KILL 33614158;
KILL 33614243;
KILL 33614253;


CREATE TABLE union2 AS SELECT l.url,
       l.id AS id,
       l.category AS category,
       l.dateadded AS dateadded,
       l.dateupdated AS date,
       l.type AS type,
       l.bedrooms AS bedrooms,
       l.bathrooms AS bathrooms,
       l.floors AS floors,
       l.price AS price,
       l.lat AS lat,
       l.lon AS lon,
       leap.epclink AS epclink,
       leap.keyFeatures AS keyFeatures,
       leap.description AS description,
       leap.agent AS agent,
       leap.agentaddress AS agentaddress,
       leap.agentphone AS agentphone,
       ah.aid AS aid,
       ah.postcode AS postcode,
       ah.thoroughfare AS thooughfare,
       ah.postTown AS postTown
       FROM rm_1000_aid ra
    INNER JOIN RM_id_aid_address rm ON ra.aid=rm.aid
    INNER JOIN valdev_3.liqquid_EA_PORT_data_2 l ON l.id = rm.id
    INNER JOIN valdev_3.liqquid_EA_PORT_data leap ON leap.id=rm.id
    INNER JOIN valdev.liqquid_addresshub ah ON ah.address1Line=rm.address;



RENAME TABLE valdev.liqquid_thoroughfare_groups TO valdev.liqquid_thoroughfare_groups_OLD;

SELECT COUNT(*) FROM valdev_2.RM_id_postcode;

CREATE TABLE liqquid_thoroughfare_groups_OLD_OCTOBER LIKE liqquid_thoroughfare_groups;
INSERT liqquid_thoroughfare_groups_OLD_OCTOBER SELECT * FROM liqquid_thoroughfare_groups;
DROP TABLE liqquid_thoroughfare_groups;
CREATE TABLE liqquid_thoroughfare_groups LIKE liqquid_thoroughfare_groups_NEW;
INSERT liqquid_thoroughfare_groups SELECT * FROM liqquid_thoroughfare_groups_NEW;
SHOW TABLES LIKE 'liqquid_EA%'