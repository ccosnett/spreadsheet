use avm;
SELECT latitude4326, longitude4326 FROM liqquid_addresshub WHERE buildingNumber='52' AND postcode='N20 9DL';

SET @t_lat = 51.6185360000;
SET @t_lon = -0.0995751000;
SET @search_radius = 0.009*3.95;
SELECT COUNT(aid) FROM liqquid_EA_PORT_data leap WHERE MBRCONTAINS(LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius)), leap.coords);

DROP TABLE IF EXISTS RM_1000;
CREATE TABLE RM_1000 AS
    SELECT
           url
         ,id
         ,category
         ,dateadded
         ,date AS dateupdated
         ,type
         ,bedrooms
         ,bathrooms
         ,floors
         ,price
         ,pcm
         ,pw
         ,title
         ,address
         ,lat
         ,lon
         ,epclink
         ,keyFeatures
         ,description
         ,agent
         ,agentaddress
         ,agentphone
         ,Type1 AS imgFloor1
         ,Type2 AS imgFloor2
         ,Type3 AS imgFloor3
         ,Type4 AS imgFloor4
         ,NULL AS imgFloor5
         ,NULL AS sqft
         ,NULL AS sqm
         ,postcode
         ,thoroughfare
         ,posttown
    FROM liqquid_EA_PORT_data l
    LEFT JOIN aids vla ON l.aid=vla.aid4
WHERE MBRCONTAINS(LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius)), l.coords)
LIMIT 1000;

SELECT * FROM RM_1000;

#SELECT * FROM liqquid_addresshub

