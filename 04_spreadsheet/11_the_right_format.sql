SELECT * FROM valdev.liqquid_addresshub WHERE aid=22406939;


use valdev_3;
SET @t_lat = 53.4134401000;
SET @t_lon = -3.0400469000;
SET @search_radius = 0.00993;
SELECT valdev_aid AS VALDEV_aid,valdev_3_aid, ah.*,valdev_aid AS valdev__aid, pp.*,valdev_aid AS valdev_aid, epc.*,valdev_aid AS valdev_aid_ FROM valdev_3.liqquid_addresshub ah
    LEFT JOIN valdev_3.pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM valdev_3.pp WHERE pp.aid=ah.aid)
    LEFT JOIN valdev_3.liqquid_epc_certs epc ON ah.aid = epc.aid
    LEFT JOIN aid_aid ON aid_aid.valdev_3_aid=ah.aid
WHERE MBRCONTAINS(LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius)), coords);

#SELECT * FROM PD
