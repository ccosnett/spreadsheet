#USE valdev_3;
use valdev_4;
SET @t_lat = 53.4134401000;
SET @t_lon = -3.0400469000;
SET @search_radius = 0.00993;
SELECT valdev_aid AS VALDEV_aid,valdev_3_aid, ah.*,valdev_aid AS valdev__aid, pp.*,valdev_aid AS valdev_aid, epc.*,valdev_aid AS valdev_aid_ FROM valdev_3.liqquid_addresshub ah
    LEFT JOIN valdev_3.pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM valdev_3.pp WHERE pp.aid=ah.aid)
    LEFT JOIN valdev_3.liqquid_epc_certs epc ON ah.aid = epc.aid
    LEFT JOIN aid_aid ON aid_aid.valdev_3_aid=ah.aid
WHERE MBRCONTAINS(LINESTRING(POINT(@t_lon + @search_radius, @t_lat + @search_radius), POINT(@t_lon - @search_radius, @t_lat - @search_radius)), coords);

USE valdev;
SELECT * FROM valdev.liqquid_epc_certs WHERE aid=916800; #P
SELECT * FROM valdev.liqquid_epc_certs WHERE aid=1112632;#S



CREATE TABLE aid_aid AS SELECT ah.aid AS valdev_aid, leanne.aid AS valdev_3_aid, leanne.address AS address FROM liqquid_ah_epc__aid_address_lmk_date leanne INNER JOIN valdev.liqquid_addresshub ah WHERE leanne.address = ah.address1Line ORDER BY ah.aid;





SELECT * FROM valdev_3.liqquid_addresshub ah LEFT JOIN valdev_3.pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM valdev_3.pp WHERE pp.aid=ah.aid) WHERE address1Line LIKE '%ch44 4dr';

SELECT count(*) FROM valdev_4.liqquid_thoroughfare_groups_3;
describe valdev_4.liqquid_thoroughfare_groups_3;

USE valdev_4;
create table  NNPAV_NHR_2 like valdev.nn_nhr;
INSERT NNPAV_NHR_2 SELECT * FROM nn_nhr;
SELECT COUNT(*) FROM valdev.NNPAV_NHR;